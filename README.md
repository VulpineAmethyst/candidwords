# Candid Words

This is a basic algorithm for generating a list of Wordle candidates based on yellow and green tiles.

## Building

You will need a C++11 compiler, for instance `g++ -std=c++11`. If you want to use a specific word list, save it as a plaintext file at 'words.txt' and run `convert.py` before compiling the program.

## Copyright

The code is made available under BSD-3-Clause.

